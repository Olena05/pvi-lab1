const socket = io('http://localhost:5000');
const messagerContainer = document.querySelector('.messager-container');
const messageContainer = document.getElementById('message-container');
const nameForm = document.getElementById('name-form');
const messageForm = document.getElementById('send-container');
const nameInput = document.getElementById('name-input');
const messageInput = document.getElementById('message-input');
const chatMembers = document.getElementById('members-icons');
const roomsList = document.getElementById('chat-rooms-list');

let currentChatRoom = null;
let userId = null;
let userName = null;
let chatRoomsList = {};
let allUsersList = {};

nameForm.addEventListener('submit', e => {
  e.preventDefault();
  userName = nameInput.value;
  socket.emit('new-user', userName);
})

socket.on('user-connected', data => {
  nameForm.classList.add('hidden');
  messagerContainer.classList.add('visible');
  const adminChatElement = addChatRoom(0, "Admin");
  adminChatElement.classList.add('selected-chat-room');
  roomsList.appendChild(adminChatElement);

  currentChatRoom = 0;
  chatRoomsList[0] = [] ;
  userId = data.id;
  allUsersList = data.usersList;

  for (let socketId in data.usersList) {
    appendMember(data.usersList[socketId], socketId);
    if (data.usersList[socketId] == userName) {
      continue;
    }
    chatRoomsList[data.usersList[socketId]] = [];
    const groupElement = addChatRoom(socketId, data.usersList[socketId]);
    roomsList.appendChild(groupElement);
  }

  data.chatHistory.forEach(message => {
    chatRoomsList[0].push(message);
  });
  displayChatHistory(chatRoomsList[0]);
});

socket.on('chat-message', data => {
  let targetRoom = data.receiverName == 0 ? 0 : data.senderName;
  if(chatRoomsList[targetRoom].length !== 0){
    chatRoomsList[targetRoom].push(data);
  }
  if (currentChatRoom == targetRoom) {
    appendMessage(data.senderName, data.messageText);
  }
  else{
    addUnreadMessagesSpan(targetRoom);
  }
});

messageForm.addEventListener('submit', e => {
  e.preventDefault();
  const message = messageInput.value;
  appendSelfMessage(message);
  const dataToSend = {senderSocketId: userId, senderName: userName, receiverName: currentChatRoom, messageText: message};
  chatRoomsList[currentChatRoom].push({ senderName: userName, messageText: message });
  socket.emit('send-chat-message', dataToSend);
  messageInput.value = '';
})

roomsList.addEventListener('click', function(event) {
  if (event.target.classList.contains('room')) {
    let id = event.target.getAttribute('data-id');
    let name = event.target.getAttribute('data-name');
    openChatRoom(id, name);
  }
});

async function openChatRoom(id, name) {
  chatRoom = id == 0 ? 0 : name;
  if (currentChatRoom == chatRoom) {
    return;
  }
  currentChatRoom = chatRoom;

  const roomElements = document.querySelectorAll('.room');
  roomElements.forEach(room => {
    if (room.getAttribute('data-name') == name) {
      room.classList.add('selected-chat-room');
      const spanElement = room.querySelector('.unread-messages-span');
      if (spanElement) {
        spanElement.remove();
      }
    } else {
      room.classList.remove('selected-chat-room');
    }
  });

  document.querySelector('.chat-name').textContent = `Chat room ${name}`;
  messageContainer.innerHTML = '';

  if (id == 0) {
    setMembers(allUsersList);
  } else {
    setMembers({ [id]: name, [userId]: userName });
  }

  if(chatRoomsList[currentChatRoom].length === 0){
    socket.emit('get-chat-history', currentChatRoom, userName);
  }
  else{
    displayChatHistory(chatRoomsList[currentChatRoom]);
  }
}

function displayChatHistory(messages){
  if (!messages) return; 
  messages.forEach(message => {
    if (message.senderName === userName) {
      appendSelfMessage(message.messageText);
    } else {
      appendMessage(message.senderName, message.messageText);
    }
  });
}

socket.on('chat-history', messages => {
  if (!messages) return; 
  messages.forEach(message => {
    chatRoomsList[currentChatRoom].push(message);
  });
  displayChatHistory(messages);
});

socket.on("new-chat", data => {
  if (chatRoomsList[data.name]){
    return;
  }
  chatRoomsList[data.name] = [];
  allUsersList[data.id] = data.name;
  let newGroup = addChatRoom(data.id, data.name);
  roomsList.append(newGroup);

  if(currentChatRoom == 0) {
    appendMember(data.name, data.id);
  }
})

function addChatRoom(id, name) {
  const groupElement = document.createElement('div');
  groupElement.classList.add('room');
  groupElement.setAttribute('data-id', id);
  groupElement.setAttribute('data-name', name);
  groupElement.innerHTML = `
    <span>
      <i class="fa-solid fa-circle-user fa-2xl" style="color: #ffffff;"></i>
    </span> 
    <span class="room-name">
      ${name}
    </span>
  `;
  return groupElement;
}

function addUnreadMessagesSpan(chatRoom){
  const roomElement = document.querySelector(`div[data-name="${chatRoom}"], div[data-id="${chatRoom}"]`);
  if (roomElement) {
    const spanElement = document.createElement('span');
    spanElement.classList.add('unread-messages-span');
    roomElement.appendChild(spanElement);
  }
}

function setMembers(listOfMembers) {
  while (chatMembers.firstChild) {
    chatMembers.removeChild(chatMembers.firstChild);
  }
  for (let socketId in listOfMembers) {
    appendMember(listOfMembers[socketId], socketId);
  }
}

function appendMember(name, id) {
  const memberElement = document.createElement('div');
  memberElement.classList.add('chat-user');
  memberElement.setAttribute('data-id', id);
  memberElement.innerHTML = `
    <span>
      <i class="fa-solid fa-circle-user fa-2xl" style="color: #b3b3b3;"></i>
    </span> 
    <span class="member-name">
      ${name}
    </span>
  `;
  chatMembers.appendChild(memberElement);
}

function appendMessage(senderName, messageText) {
  const messageElement = document.createElement('div');
  messageElement.classList.add('message-box');
  messageElement.innerHTML = `
    <div class="chat-user">
      <span>
        <i class="fa-solid fa-circle-user fa-2xl" style="color: #b3b3b3;"></i>
      </span> 
      <span class="member-name">
        ${senderName}
      </span>
    </div>
    <div class="chat-message">
      <span>${messageText}</span>
    </div>
  `;
  messageContainer.appendChild(messageElement);
}

function appendSelfMessage(messageText) {
  const messageElement = document.createElement('div');
  messageElement.classList.add('message-box', 'self-message-box');
  messageElement.innerHTML = `
    <div class="chat-message self-message">
      <span>${messageText}</span>
    </div>
    <div class="chat-user">
      <span>
        <i class="fa-solid fa-circle-user fa-2xl" style="color: #b3b3b3;"></i>
      </span> 
      <span class="member-name">
        You
      </span>
    </div>
  `;  
  messageContainer.appendChild(messageElement);
}
